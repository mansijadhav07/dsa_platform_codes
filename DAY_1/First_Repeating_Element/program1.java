/*
Code2: First Repeating Element

Company: Amazon, oracle

Platform: GFG

Striver’s SDE Sheet

Description:

Given an array arr[] of size n, find the first repeating element. The element
should occur more than once and the index of its first occurrence should be the
smallest.

Note:- The position you return should be according to 1-based indexing.

Example 1:
Input:
n = 7
arr[] = {1, 5, 3, 4, 3, 5, 6}
Output: 2
Explanation:
5 is appearing twice and
its first appearance is at index 2
which is less than 3 whose first
occurring index is 3.

Example 2:
Input:
n = 4
arr[] = {1, 2, 3, 4}
Output: -1
Explanation:
All elements appear only once so
the answer is -1.

Expected Time Complexity: O(n)
Expected Auxiliary Space: O(n)

Constraints:
1 <= n <= 106
0 <= Ai<= 106*/
import java.io.*;
import java.util.*;
class Solution{
	static int FirstRepeatingEle(int[] arr,int n){
		int idx=-1;
		HashSet<Integer> hs=new HashSet<Integer>();
		for(int i=n-1;i>=0;i--){
			if(hs.contains(arr[i])){
				idx=i;
			}else{
				hs.add(arr[i]);
			}
		}
		if(idx==-1){
			return idx;
		}else{
			return idx+1;
		}
	}
	public static void main(String[] args)throws IOException{
		System.out.println("Enter size of array");
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size=Integer.parseInt(br.readLine());
		int[] arr=new int[size];
		System.out.println("Enter array elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		Solution obj=new Solution();
		int ret=obj.FirstRepeatingEle(arr,size);
		System.out.println("First Repeating element:"+ret);
	}
}
