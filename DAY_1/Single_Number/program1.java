/*
Code 1:Single Number

Company : Amazon, wipro, Capgemini, DXC technology, Schlumberger,
Avizva, epam, cadence, paytm, atlassian,cultfit+7

Platform: LeetCode - 136

Striver’s SDE Sheet

Description:
Given a non-empty array of integers nums, every element appears
twice except for one. Find that single one.
You must implement a solution with a linear runtime complexity and use
only constant extra space.

Example 1:
Input: nums = [2,2,1]
Output: 1

Example 2:
Input: nums = [4,1,2,1,2]
Output: 4

Example 3:
Input: nums = [1]
Output: 1

Constraints:
1 <= nums.length <= 3 * 10^4
-3 * 104 <= nums[i] <= 3 * 10^4
Each element in the array appears twice except for one element
which appears only once.
*/
import java.io.*;
class Solution {
    public int singleNumber(int[] nums) {
        int ans = -1;
        for (int i = 0; i < nums.length; i++) {
            int c = 0;
            int e = nums[i];
            for (int j = 0; j < nums.length; j++) {
                if (e == nums[j]) {
     			c = c + 1;
                   }
            }
            if (c == 1) {
                ans = nums[i];
	    }

        }
        return ans;
    }
    public static void main(String[] args)throws IOException{
	    BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	    System.out.println("Enter size of array");
	    int size=Integer.parseInt(br.readLine());
	    int[] nums=new int[size];
	    System.out.println("Enter array elements");
	    for(int i=0;i<size;i++){
		    nums[i]=Integer.parseInt(br.readLine());
	    }
	    Solution obj=new Solution();
	    int ret=obj.singleNumber(nums);
	    System.out.println("single element:"+ret);
    }

}
