/*
Code 3:Minimum distance between two numbers
Companies : Amazon, Paytm
Platform: GFG

You are given an array a, of n elements. Find the minimum index based distance
between two distinct elements of the array, x and y. Return -1, if either x or y
does not exist in the array.
Example 1:
Input:
N = 4
A[] = {1,2,3,2}
x = 1, y = 2
Output: 1

Explanation: x = 1 and y = 2. There are
two distances between x and y, which are
1 and 3 out of which the least is 1.
Example 2:
Input:
N = 7
A[] = {86,39,90,67,84,66,62}
x = 42, y = 12
Output: -1
Explanation: x = 42 and y = 12. We return
-1 as x and y don't exist in the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 105
0 <= a[i], x, y <= 105
x != y*/
import java.io.*;
class Solution {
    int minDist(int a[], int n, int x, int y) {
        int ans=Integer.MAX_VALUE;
        int first=-1;
        int second=-1;
        for(int i=0;i<n;i++){
            if(a[i]==x){
                first=i;

            }
            if(a[i]==y){
                second=i;

            }
            if(first!=-1 && second!=-1){
                ans=Math.min(Math.abs(first-second),ans);
            }

        }

        if(first==-1 || second==-1){
            ans= -1;
        }
        return ans;

    }
     public static void main(String[] args)throws IOException{
                System.out.println("Enter size of array");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size=Integer.parseInt(br.readLine());
                int[] arr=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		System.out.println("Enter value of x");
		int x=Integer.parseInt(br.readLine());
		System.out.println("Enter value of y");
                int y=Integer.parseInt(br.readLine());
                Solution obj=new Solution();
                int ret=obj.minDist(arr,size,x,y);
                System.out.println("minimum distance:"+ret);
        }
}
