/*
Code 1: Plus One
Companies: Microsoft, Google
Platform: LeetCode - 66

Fraz’s SDE sheet.
You are given a large integer represented as an integer array digits, where each
digits[i] is the ith digit of the integer. The digits are ordered from most significant
to least significant in left-to-right order. The large integer does not contain any
leading 0's.
Increment the large integer by one and return the resulting array of digits.
Example 1:
Input: digits = [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
Incrementing by one gives 123 + 1 = 124.
Thus, the result should be [1,2,4].
Example 2:
Input: digits = [4,3,2,1]
Output: [4,3,2,2]
Explanation: The array represents the integer 4321.
Incrementing by one gives 4321 + 1 = 4322.
Thus, the result should be [4,3,2,2].
Example 3:
Input: digits = [9]
Output: [1,0]
Explanation: The array represents the integer 9.
Incrementing by one gives 9 + 1 = 10.
Thus, the result should be [1,0].

Constraints:
1 <= digits.length <= 100
0 <= digits[i] <= 9
digits does not contain any leading 0's.*/

import java.io.*;
class Solution {
    public int[] plusOne(int[] digits) {
       for(int i=digits.length-1;i>=0;i--){
                if(digits[i]==9){
                    digits[i]=0;
                }else{
                    digits[i]++;
                    return digits;
                }
        }

        int[] ans=new int[digits.length+1];
        ans[0]=1;
        return ans;
    }
    public static void main(String[] args)throws IOException{
                System.out.println("Enter size of array");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size=Integer.parseInt(br.readLine());
                int[] arr=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                Solution obj=new Solution();
                obj.plusOne(arr);
		System.out.println("After increment plus one");
                for(int i=0;i<size;i++){
                        System.out.print(arr[i]+" ");
                }
		System.out.println();
        }

}
