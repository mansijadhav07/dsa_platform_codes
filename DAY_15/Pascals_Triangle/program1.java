/*
Given an integer numRows, return the first numRows of Pascal's triangle.
Example 1:

Input: numRows = 5
Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
Example 2:

Input: numRows = 1
Output: [[1]]


Constraints:

1 <= numRows <= 30*/
import java.io.*;
import java.util.*;
class Solution {
    public List<List<Integer>> generate(int numRows) {
           List<List<Integer>> ans=new ArrayList<>();
            List<Integer> firstRow=new ArrayList<Integer>();
            firstRow.add(1);
            ans.add(firstRow);
            if(numRows==0){
                return ans;
            }
            for(int i=1;i<numRows;i++){
                    List<Integer>prev=ans.get(i-1);
                    List<Integer>row=new ArrayList();
                    row.add(1);

                for(int j=1;j<i;j++){
                    row.add(prev.get(j-1)+prev.get(j));
                 }
                row.add(1);
                ans.add(row);
                }
                return ans;
    }
    public static void main(String[] args)throws IOException{
                System.out.println("Enter number of rows");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int numRows=Integer.parseInt(br.readLine());
		List<List<Integer>> ans=new ArrayList<>();
		Solution obj=new Solution();
		ans=obj.generate(numRows);
		System.out.println(ans);
    }

}
