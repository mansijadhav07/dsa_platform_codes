import java.io.*;
class Solution
{
    public void transpose(int n,int a[][])
    {
        int[][] ans=new int[n][n];
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                ans[i][j]=a[i][j];
            }
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                a[j][i]=ans[i][j];
            }
        }
    }
    public static void main(String args[])throws IOException
    {
	System.out.println("Enter array size");
        BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
        int n=Integer.parseInt(in.readLine());
        int a[][]=new int[n][n];
	System.out.println("Enter array elements");
        for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
                    a[i][j]=Integer.parseInt(in.readLine());
		}
	}
	 Solution obj=new Solution();
         obj.transpose(n,a);
         for(int i=0;i<n;i++){
              for(int j=0;j<n;j++){
                    System.out.print(a[i][j]+" ");
              }
	      System.out.println();
	 }
    }
}
