/*Code 1: Best Time to Buy and Sell Stock
Company: Amazon, Google, Flipkart, Microsoft, PayU, Phone Pe, American
Express, Cognizant, Cisco, Dunzo
Platform: Leetcode - 121
Fraz’s, Striver’s and love bubbar’s DSA sheet.
Description:
You are given an array of prices where prices[i] is the price of a given stock
on the ith day.
You want to maximize your profit by choosing a single day to buy one stock and
choosing a different day in the future to sell that stock.
Return the maximum profit you can achieve from this transaction. If you cannot
achieve any profit, return 0.
Example 1:
Input: prices = [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit =
6-1 = 5.
Note that buying on day 2 and selling on day 1 is not allowed because you
must buy before you sell.
Example 2:
Input: prices = [7,6,4,3,1]
Output: 0
Explanation: In this case, no transactions are done and the max profit = 0.
Constraints:
1 <= prices.length <= 105
0 <= prices[i] <= 104*/
import java.io.*;
class Solution {
    public int maxProfit(int[] prices) {
        int buy=0;
        int sell=0;
        int min=Integer.MAX_VALUE;
        int total=Integer.MIN_VALUE;
        for(int i=0;i<prices.length;i++){
            if(min>prices[i]){
                min=prices[i];
            }
            if(total<prices[i]-min){
                total=prices[i]-min;
            }
        }
        return total;
    }
    public static void main(String[] args)throws IOException{
                System.out.println("Enter size of array");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size=Integer.parseInt(br.readLine());
                int[] arr=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                Solution obj=new Solution();
                int ret=obj.maxProfit(arr);
                System.out.println("Max profit:"+ret);
        }
}
