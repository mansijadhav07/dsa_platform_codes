/*
Code3 : Find the smallest and second smallest element in
an array
Company: Amazon, Goldman Sachs
Platform: GFG
Description:
Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
2 4 3 5 6
Output :
2 3
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.

Example 2:
Input :
6
1 2 1 3 6 7
Output :
1 2
Explanation:
1 and 2 are respectively the smallest
and second smallest elements in the array.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
1<=N<=105
1<=A[i]<=105*/
import java.io.*;
import java.util.*;
class Solution{
	int[] SmallestAndSecondSmallest(int[] arr,int size){
		int[] ans=new int[2];
		Arrays.sort(arr);
		ans[0]=arr[0];
                 
		int j=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]!=ans[0]){
				ans[1]=arr[i];
				break;
			}
			j++;
		}
		if(j==size-1){
			ans[0]=ans[1]=-1;
		}
		return ans;
	}
	public static void main(String[] args)throws IOException{
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter size of array");
            int size=Integer.parseInt(br.readLine());
            int[] nums=new int[size];
            System.out.println("Enter array elements");
            for(int i=0;i<size;i++){
                    nums[i]=Integer.parseInt(br.readLine());
            }
            Solution obj=new Solution();
            int[] ret=obj.SmallestAndSecondSmallest(nums,size);
            System.out.println("Smallest and second smallest element are :"+Arrays.toString(ret));
    }
}
