/*
Code 3: Even and Odd

Company: Paytm, Amazon, Microsoft
Platform: GFG
Description:
Given an array arr[] of size N containing equal number of odd and even numbers.
Arrange the numbers in such a way that all the even numbers get the even index and
odd numbers get the odd index.
Note: There are multiple possible solutions, Print any one of them. Also, 0-based
indexing is considered.

Example 1:
Input:
N = 6
arr[] = {3, 6, 12, 1, 5, 8}
Output:
1
Explanation:
6 3 12 1 8 5 is a possible solution.
The output will always be 1 if your
rearrangement is correct.

Example 2:
Input:
N = 4
arr[] = {1, 2, 3, 4}
Output :
1
Your Task:
You don't need to read input or print anything. Your task is to complete the function
reArrange() which takes an integer N and an array arr of size N as input and reArranges
the array in Place without any extra space.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
1 ≤ N ≤ 105
1 ≤ arr[i] ≤ 105*/
import java.io.*;
import java.util.*;

class Solution {
    public static void swap(int arr[],int a,int b){
        int temp=arr[a];
        arr[a]=arr[b];
        arr[b]=temp;
    }
    static void reArrange(int[] arr, int N) {
        // code here
        int i=0,j=1;
        while(i<arr.length && j<arr.length){
            if(arr[i]%2==0){
                i=i+2;
            }else if(arr[j]%2==1){
                j=j+2;
            }else if(arr[i]%2!=0 && arr[j]%2!=1){
                swap(arr,i,j);
                i=i+2;
                j=j+2;
            }
        }
    }
};

class Client {
    public static void main(String args[]) throws IOException {
        BufferedReader read =
            new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter array size");
            int N = Integer.parseInt(read.readLine());

            
            int[] arr = new int[N];
            System.out.println("Enter array elements");
            for(int i=0; i<N; i++){
                arr[i] = Integer.parseInt(read.readLine());
	    }

            Solution ob = new Solution();
            ob.reArrange(arr,N);

            System.out.println(check(arr,N));
        }
    
    static int check(int arr[], int n)
    {
        int flag = 1;
        int c=0, d=0;
        for(int i=0; i<n; i++)
        {
            if(i%2==0)
            {
                if(arr[i]%2==1)
                {
                    flag = 0;
                    break;
                }
                else
                    c++;
            }
            else
            {
                if(arr[i]%2==0)
                {
                    flag = 0;
                    break;
                }
                else
                    d++;
            }
        }
        if(c!=d)
            flag = 0;

        return flag;
    }
}

