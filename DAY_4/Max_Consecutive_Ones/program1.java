/*
Easy Level - Day 4

Code 1: Max Consecutive Ones

Company: Google, Facebook, Amazon, Microsoft, Apple, Uber, Airbnb, Adobe, Goldman Sachs,
Bloomberg

Platform: Leetcode - 485, Coding Ninja
Striver’s SDE Sheet

Description:
Given a binary array nums, return the maximum number of consecutive 1's in the array.

Example 1:
Input: nums = [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s. The
maximum number of consecutive 1s is 3.

Example 2:
Input: nums = [1,0,1,1,0,1]
Output: 2

Constraints:
1 <= nums.length <= 105
nums[i] is either 0 or 1.*/

import java.io.*;
class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        int count1=0;
        int count2=0;
        for(int i=0;i<nums.length;i++){
            if(nums[i]==1){
                count1++;
            }else{
                if(count1>count2){
                    count2=count1;
                }else{
                    count2=count2;
                }

                if(nums[i]==0){
                    count1=0;
                }
            }
        }
        if(count1>count2){
            count2=count1;
        }
        return count2;

    }
    public static void main(String[] args)throws IOException{
                System.out.println("Enter size of array");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size=Integer.parseInt(br.readLine());
                int[] arr=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		
                Solution obj=new Solution();
                int ret=obj.findMaxConsecutiveOnes(arr);
                System.out.println("max consecutive ones in given array :"+ret);
        }
}


