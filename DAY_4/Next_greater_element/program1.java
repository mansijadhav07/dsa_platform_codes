/*
Code 2: Next Greater Element I

Company: Flipkart, Amazon, Microsoft, MakeMyTrip, Adobe
Platform: Leetcode - 496, GFG
Description:

The next greater element of some element x in an array is the first greater element that
is to the right of x in the same array.
You are given two distinct 0-indexed integer arrays nums1 and nums2, where nums1 is a
subset of nums2.
For each 0 <= i < nums1.length, find the index j such that nums1[i] == nums2[j] and
determine the next greater element of nums2[j] in nums2. If there is no next greater
element, then the answer for this query is -1.
Return an array ans of length nums1.length such that ans[i] is the next greater element
as described above.

Example 1:
Input: nums1 = [4,1,2], nums2 = [1,3,4,2]
Output: [-1,3,-1]
Explanation: The next greater element for each value of nums1 is as follows:
- 4 is underlined in nums2 = [1,3,4,2]. There is no next greater element, so the
answer is -1.
- 1 is underlined in nums2 = [1,3,4,2]. The next greater element is 3.
- 2 is underlined in nums2 = [1,3,4,2]. There is no next greater element, so the
answer is -1.

Example 2:
Input: nums1 = [2,4], nums2 = [1,2,3,4]
Output: [3,-1]
Explanation: The next greater element for each value of nums1 is as follows:
- 2 is underlined in nums2 = [1,2,3,4]. The next greater element is 3.
- 4 is underlined in nums2 = [1,2,3,4]. There is no next greater element, so the
answer is -1.

Constraints:
1 <= nums1.length <= nums2.length <= 1000
0 <= nums1[i], nums2[i] <= 104
All integers in nums1 and nums2 are unique.
All the integers of nums1 also appear in nums2.*/

import java.io.*;
import java.util.*;
class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int ans[]=new int[nums1.length];
        for(int i=0;i<nums1.length;i++){
            ans[i]=-1;
        }
        for(int i=0;i<nums1.length;i++){
            int store=nums1[i];
            for(int j=0;j<nums2.length;j++){
                if(store==nums2[j]){
                for(int k=j+1;k<nums2.length;k++){
                   if(store<nums2[k]){
                        ans[i]=nums2[k];
                        break;
                   }
                }
                }
            }
        }

        return ans;
    }
    public static void main(String[] args)throws IOException{
                System.out.println("Enter size of array1");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size1=Integer.parseInt(br.readLine());
                int[] arr1=new int[size1];
                System.out.println("Enter array1 elements");
                for(int i=0;i<size1;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }
		System.out.println("Enter size of array2");
                int size2=Integer.parseInt(br.readLine());
                int[] arr2=new int[size2];
                System.out.println("Enter array elements2");
                for(int i=0;i<size2;i++){
                        arr2[i]=Integer.parseInt(br.readLine());
                }
		
                Solution obj=new Solution();
                int[] ret=obj.nextGreaterElement(arr1,arr2);
                System.out.println("output array:"+Arrays.toString(ret));
        }
}


