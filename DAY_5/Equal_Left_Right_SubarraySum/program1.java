/*
Code2 : Equal Left and Right Subarray Sum
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n positive numbers. The task is to find the first index in the
array such that the sum of elements before it equals the sum of elements after it.
Note: Array is 1-based indexed.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: For second test case at position 3 elements before it (1+3) =
elements after it (2+2).

Example 2:
Input:

n = 1
A[] = {1}
Output: 1
Explanation: Since it's the only element hence it is the only point.
Expected Time Complexity: O(N)
Expected Space Complexity: O(1)
Constraints:
1 <= n <= 106
1 <= A[i] <= 108*/

import java.io.*;
class Solution{
	int equalSum(int [] A, int N) {
		int sum = 0;
		int leftSum = 0;
		for(int i=0; i<A.length; i++){
		    sum = sum + A[i];
		}
		
		int result = -1 ;
        for(int i=0; i<A.length; i++){
            leftSum = leftSum + A[i];
            if((i==0 && leftSum == sum)){
                result = 1;
                break;
            }else if((i==A.length-1 && sum == 0)){
                result = i+1;
                break;
            }else if(i>0 && (sum - leftSum == leftSum - A[i])){
                result = i+1;
                break;
            }
        }
      return result;
	}
	 public static void main(String[] args)throws IOException{
                System.out.println("Enter size of array");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size=Integer.parseInt(br.readLine());
                int[] arr=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                Solution obj=new Solution();
                int ret=obj.equalSum(arr,size);
                System.out.println("equal left and right subarray sum :"+ret);
        }
}


