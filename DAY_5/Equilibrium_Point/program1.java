/*Code3 : Equilibrium Point
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n non negative numbers. The task is to find the first
equilibrium point in an array. Equilibrium point in an array is an index (or position) such
that the sum of all elements before that index is the same as the sum of elements after
it.
Note: Return equilibrium point in 1-based indexing. Return -1 if no such point exists.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: The equilibrium point is at position 3 as the sum of elements
before it (1+3) = sum of elements after it (2+2).
Example 2:

Input:
n = 1
A[] = {1}
Output: 1
Explanation: Since there's only an element hence its only the equilibrium point.
Expected Time Complexity: O(n)
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 105
0 <= A[i] <= 109*/
import java.io.*;
class Solution {
    public static int equilibriumPoint(int arr[], int n) {

        int sum = 0;
	int leftSum = 0;
	for(int i=0; i<arr.length; i++){
		    sum = sum + arr[i];
	}
	int result = -1 ;
        for(int i=0; i<arr.length; i++){
            leftSum = leftSum + arr[i];
            if((i==0 && leftSum == sum)){
                result = 1;
                break;
            }else if((i==arr.length-1 && sum == 0)){
                result = i+1;
                break;
            }else if(i>0 && (sum - leftSum == leftSum - arr[i])){
                result = i+1;
                break;
            }
        }
        return result;
    }
    public static void main(String[] args)throws IOException{
                System.out.println("Enter size of array");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size=Integer.parseInt(br.readLine());
                int[] arr=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		
                Solution obj=new Solution();
                int ret=obj.equilibriumPoint(arr,size);
                System.out.println("Equilibrium point:"+ret);
        }

}

