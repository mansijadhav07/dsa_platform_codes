/*
Three Great Candidates/ Three Ninja Candidates/
Maximum Product of Three Numbers
Company: Flipkart, Amazon, Snapdeal
Platform: Leetcode- 628, GFG, Coding Ninja
Fraz’s SDE Sheet
Description :
The hiring team aims to find 3 candidates who are great collectively. Each
candidate has his or her ability expressed as an integer. 3 candidates are great
collectively if the product of their abilities is maximum. Given abilities of N candidates in
an array arr[], find the maximum collective ability from the given pool of candidates.
Example 1:
Input:
N = 5
Arr[] = {10, 3, 5, 6, 20}
Output: 1200
Explanation:The multiplication of 10, 6 and 20 is 1200.

Example 2:
Input:
N = 5
Arr[] = {-10, -3, -5, -6, -20}
Output: -90
Explanation:
Multiplication of -3, -5 and -6 is -90.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
3 ≤ N ≤ 107
-105 ≤ Arr[i] ≤ 105*/
import java.io.*;
import java.util.*;
class Solution {
    public int maximumProduct(int[] nums) {
        int ret1=1;
        int ret2=1;
        Arrays.sort(nums);
        ret1=ret1*nums[nums.length-1]*nums[nums.length-2]*nums[nums.length-3];

        ret2=ret2*nums[nums.length-1]*nums[0]*nums[1];
        if(ret1>ret2){
            return ret1;
        }else{
            return ret2;
        }

    }
    public static void main(String[] args)throws IOException{
                System.out.println("Enter size of array");
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                int size=Integer.parseInt(br.readLine());
                int[] arr=new int[size];
                System.out.println("Enter array elements");
                for(int i=0;i<size;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                Solution obj=new Solution();
                int ret=obj.maximumProduct(arr);
                System.out.println("Maximum product of three numbers:"+ret);
      }

}


